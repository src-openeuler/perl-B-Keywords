%bcond_with perl_B_Keywords_enables_extra_test
Name:                perl-B-Keywords
Version:             1.27
Release:             2
Summary:             Lists of reserved barewords and symbol names
License:             GPL+ or Artistic
URL:                 https://metacpan.org/release/B-Keywords
Source0:             https://cpan.metacpan.org/modules/by-module/B/B-Keywords-%{version}.tar.gz
BuildArch:           noarch
BuildRequires:       coreutils findutils make perl-generators perl-interpreter perl(Config)
BuildRequires:       perl(ExtUtils::MakeMaker) perl(Exporter) perl(strict) perl(vars) perl-devel
BuildRequires:       perl(File::Spec) perl(lib) perl(Test) perl(Test::More) perl(Test::Pod) >= 1.0
%if 0%{!?perl_bootstrap:1} && %{with perl_B_Keywords_enables_extra_test}
BuildRequires:       perl(File::Copy) perl(Perl::MinimumVersion) >= 1.20
BuildRequires:       perl(Test::CPAN::Meta) >= 0.12 perl(Test::Kwalitee)
BuildRequires:       perl(Test::MinimumVersion) >= 0.008 perl(Test::More) >= 0.88
BuildRequires:       perl(Test::Pod::Coverage) >= 1.04 perl(warnings)
%endif
%description
Keyword provides an array of several exportable keywords

%prep
%setup -q -n B-Keywords-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
make pure_install DESTDIR=%{buildroot}
find %{buildroot} -type f -name .packlist -delete
%{_fixperms} -c %{buildroot}

%check
# TODO: Need somebody fix fail
%if 0%{!?perl_bootstrap:1} && %{with perl_B_Keywords_enables_extra_test}
make test IS_MAINTAINER=1 AUTHOR_TESTING=1
%else
make test
%endif

%files
%license LICENSE
%doc Changes
%{perl_vendorlib}/B/
%{_mandir}/man3/B::Keywords.3*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 1.27-2
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Jul 23 2024 gss <guoshengsheng@kylinos.cn> - 1.27-1
- Upgrade to version 1.27
- Add bareword __CLASS__ since 5.39.2

* Tue Mar 19 2024 yaoxin <yao_xin001@hoperun.com> - 1.26-2
- Enable test

* Fri Jul 07 2023 leeffo <liweiganga@uniontech.com> - 1.26-1
- upgrade to version 1.26

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.24-1
- Upgrade to version 1.24

* Fri Jun 25 2021 Wenlong Ding <wenlong.ding@turbolinux.com.cn> 1.20-2
- Reformat spec file, replace '\r\n' to '\n'.

* Mon May 17 2021 Pengju Jiang <jiangpengju2@huawei.com> - 1.20-1
- package init

